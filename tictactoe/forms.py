__author__ = 'jramirez'
from django import forms
from .models import Invitation


class InvitationForm(forms.ModelForm):
    class Meta:
        model = Invitation
        fields = '__all__'